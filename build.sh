#!/bin/sh
###################################
# Take LTE Active Kernel Source   #             
#           bestmjh47             #
###################################

# GCC sets
TOOLCHAINPATH=$HOME/toolchain/linaro_4.8.4-a9/bin
export ARCH=arm
export CROSS_COMPILE=$TOOLCHAINPATH/arm-eabi-

# Set defconfig
make bestmjh47_defconfig

# Build
make -j15
cp arch/arm/boot/zImage zImage

# Clean previous directory & Module-related scripts
rm -rf Modules
mkdir Modules
find -name '*.ko' -exec cp -av {} Modules \;
        for i in Modules/*; do $TOOLCHAINPATH/arm-eabi-strip --strip-unneeded $i;done;\

# Prima-wlan related scripts
mkdir -p Modules/etc/firmware/wlan/prima
mkdir -p ramdisk/system/etc/firmware/wlan/prima
cp -f Modules/cfg80211.ko ramdisk/system/lib/modules/prima/cfg80211.ko
cp -f Modules/wlan.ko ramdisk/system/lib/modules/prima/prima_wlan.ko
cp -f drivers/staging/prima/firmware_bin/WCNSS_cfg.dat ramdisk/system/etc/firmware/wlan/prima/WCNSS_cfg.dat

# Boot image build script
ramdisk/mkbootimg --cmdline "androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x3F ehci-hcd.park=3 no_console_suspend=1" --base 0x80200000 --pagesize 2048 --ramdiskaddr 0x82200000 --kernel zImage --ramdisk ramdisk/bestmjh47-ramdisk.gz -o ramdisk/boot.img

# Build flashable zip
cd ramdisk
zip -r Active-E100-bestmjh47_kernel.zip META-INF system boot.img
mv -v Active-E100-bestmjh47_kernel.zip ../

